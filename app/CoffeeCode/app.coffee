express        = require "express"
morgan         = require "morgan"
bodyParser     = require "body-parser"
methodOverride = require "method-override"
app            = express()
http		   = require "http" 
path		   = require 'path'

configurator = require path.join __dirname, "route/routing.compiled"
configurator app

app.use express.static "public"     # set the static files location /public/img will be /img for users
app.use morgan "dev"                     # log every request to the console
app.use bodyParser.urlencoded { extended: false }    # parse application/x-www-form-urlencoded
app.use bodyParser.json     # parse application/json
app.use methodOverride                   # simulate DELETE and PUT

app.set "views", "views"
app.set "view engine", "jade"
app.enable "view cache"

port = parseInt process.env.PORT or 3000, 10
app.set "port", port

app.listen port
console.log "Listening on port #{port}"



