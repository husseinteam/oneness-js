qapi = require "../../api/quiz.api.compiled"
express = require "express"

module.exports = (app) ->
	quiz = express.Router()
	quiz.get '/shuffle', (req, res, next) ->
		qapi (data, response) ->
			res.render 'quiz', 
				Model: data,
				ViewModel: 
					ApplicationName: 	"Oneness"
					Slogan: 			"One for All Whole for One!"
					ApplicationTitle: 	"Welcome to us! To Oneness" 
					DeveloperNick: 		"lampiclobe"
					CancelSubmit: 		"Cancel"
					SaveSubmit:			"Save"
					CancelSubmitPop:	"Send Your Response"
					PleaseFindAMatch: 	"Please Find a match"
					BeWithUs:			"Be with us!"
					Jumbotron:			"Here you are as a privacy. From now on everything gonna change!"
					QuestionTitle:		"Question"
			next()
	app.use("/quiz", quiz)