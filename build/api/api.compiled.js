(function() {
  var Config, rest;

  Config = require("" + __dirname + "/Config/config.compiled");

  rest = require("restler");

  module.exports = function(parameters, callback) {
    var path;
    path = "" + Config.ApiHost + "/" + parameters.controller + "/" + parameters.route;
    return rest.get(path, {
      timeout: 10000
    }).on("timeout", function(ms) {
      return console.log("did not return within " + ms + " ms");
    }).on("complete", callback);
  };

}).call(this);
