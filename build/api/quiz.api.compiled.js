(function() {
  var api;

  api = require("" + __dirname + "/api.compiled");

  module.exports = function(callback) {
    var args, parameters;
    args = {
      headers: {
        'test-header': 'client-api'
      }
    };
    parameters = {
      args: args,
      route: 'shuffle',
      method: 'GET',
      controller: 'quiz'
    };
    return api(parameters, callback);
  };

}).call(this);
