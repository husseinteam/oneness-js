(function() {
  var express, qapi;

  qapi = require("../../api/quiz.api.compiled");

  express = require("express");

  module.exports = function(app) {
    var quiz;
    quiz = express.Router();
    quiz.get('/shuffle', function(req, res, next) {
      return qapi(function(data, response) {
        res.render('quiz', {
          Model: data,
          ViewModel: {
            ApplicationName: "Oneness",
            Slogan: "One for All Whole for One!",
            ApplicationTitle: "Welcome to us! To Oneness",
            DeveloperNick: "lampiclobe",
            CancelSubmit: "Cancel",
            SaveSubmit: "Save",
            CancelSubmitPop: "Send Your Response",
            PleaseFindAMatch: "Please Find a match",
            BeWithUs: "Be with us!",
            Jumbotron: "Here you are as a privacy. From now on everything gonna change!",
            QuestionTitle: "Question"
          }
        });
        return next();
      });
    });
    return app.use("/quiz", quiz);
  };

}).call(this);
