(function() {
  module.exports = function(app) {
    var indexRoute, quizRoute;
    quizRoute = require("" + __dirname + "/quiz.route.compiled");
    indexRoute = require("" + __dirname + "/index.route.compiled");
    return [indexRoute, quizRoute].map(function(route) {
      return route(app);
    });
  };

}).call(this);
