(function() {
  var express;

  express = require("express");

  module.exports = function(app) {
    var index;
    index = express.Router();
    index.get('/', function(req, res, next) {
      ({
        ViewModel: {
          ApplicationName: "Oneness",
          Slogan: "One for All Whole for One!",
          ApplicationTitle: "Welcome to us! To Oneness",
          DeveloperNick: "lampiclobe",
          CancelSubmit: "Cancel",
          SaveSubmit: "Save",
          CancelSubmitPop: "Send Your Response",
          PleaseFindAMatch: "Please Find a match",
          BeWithUs: "Be with us!",
          Jumbotron: "Here you are as a privacy. From now on everything gonna change!",
          QuestionTitle: "Question"
        }
      });
      return next();
    });
    return app.use("/", index);
  };

}).call(this);
