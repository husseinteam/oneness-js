(function() {
  var app, bodyParser, configurator, express, http, methodOverride, morgan, path, port;

  express = require("express");

  morgan = require("morgan");

  bodyParser = require("body-parser");

  methodOverride = require("method-override");

  app = express();

  http = require("http");

  path = require('path');

  configurator = require(path.join(__dirname, "route/routing.compiled"));

  configurator(app);

  app.use(express["static"]("public"));

  app.use(morgan("dev"));

  app.use(bodyParser.urlencoded({
    extended: false
  }));

  app.use(bodyParser.json);

  app.use(methodOverride);

  app.set("views", "views");

  app.set("view engine", "jade");

  app.enable("view cache");

  port = parseInt(process.env.PORT || 3000, 10);

  app.set("port", port);

  app.listen(port);

  console.log("Listening on port " + port);

}).call(this);
