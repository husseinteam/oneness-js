
Config = require "#{__dirname}/Config/config.compiled"
rest = require "restler"

module.exports = (parameters, callback) ->
	path = "#{Config.ApiHost}/#{parameters.controller}/#{parameters.route}"
	rest.get path, {timeout: 10000}
		.on "timeout", (ms) ->
			console.log "did not return within #{ms} ms"
		.on "complete", callback
