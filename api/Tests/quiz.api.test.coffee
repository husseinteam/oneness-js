quizApi = require "../build/api/quiz.api.compiled"

assert = require "assert" 

describe "quizApi", () ->
	it "should rest api", (done) ->
		this.timeout(10000)
		quizApi (data, response) ->
			assert !!data, "!data" 
			assert response.statusCode == 200, "invaid response"
			quiz = data
			assert !!quiz.Manifest, "!quiz.Manifest" 
			assert !!quiz.Manifest.Sentences, "!quiz.Manifest.Sentences" 
			assert !!quiz.Questions, "!quiz.Questions" 
			for question in quiz.Questions
				do (question) -> 
					assert !!question, "!question" 
					assert !!question.Manifest, "!question.Manifest" 
					for sentence in question.Manifest.Sentences
						do (sentence) -> 
							assert !!sentence, "!!sentence"
							assert !!sentence.Language, "!!sentence.Language"
					assert !!question.Answers, "!question.Answers" 
					for answer in question.Answers
						do (answer) -> 
							assert !!answer, "!answer" 
							assert !!answer.Manifest, "!answer.Manifest" 
			done()
