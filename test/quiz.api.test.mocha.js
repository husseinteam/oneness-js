(function() {
  var assert, quizApi;

  quizApi = require("../build/api/quiz.api.compiled");

  assert = require("assert");

  describe("quizApi", function() {
    return it("should rest api", function(done) {
      this.timeout(10000);
      return quizApi(function(data, response) {
        var question, quiz, _fn, _i, _len, _ref;
        assert(!!data, "!data");
        assert(response.statusCode === 200, "invaid response");
        quiz = data;
        assert(!!quiz.Manifest, "!quiz.Manifest");
        assert(!!quiz.Manifest.Sentences, "!quiz.Manifest.Sentences");
        assert(!!quiz.Questions, "!quiz.Questions");
        _ref = quiz.Questions;
        _fn = function(question) {
          var answer, sentence, _fn1, _j, _k, _len1, _len2, _ref1, _ref2, _results;
          assert(!!question, "!question");
          assert(!!question.Manifest, "!question.Manifest");
          _ref1 = question.Manifest.Sentences;
          _fn1 = function(sentence) {
            assert(!!sentence, "!!sentence");
            return assert(!!sentence.Language, "!!sentence.Language");
          };
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            sentence = _ref1[_j];
            _fn1(sentence);
          }
          assert(!!question.Answers, "!question.Answers");
          _ref2 = question.Answers;
          _results = [];
          for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
            answer = _ref2[_k];
            _results.push((function(answer) {
              assert(!!answer, "!answer");
              return assert(!!answer.Manifest, "!answer.Manifest");
            })(answer));
          }
          return _results;
        };
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          question = _ref[_i];
          _fn(question);
        }
        return done();
      });
    });
  });

}).call(this);
